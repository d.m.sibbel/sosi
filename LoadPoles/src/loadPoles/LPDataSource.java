package loadPoles;

import repast.simphony.data2.NonAggregateDataSource;

public class LPDataSource implements NonAggregateDataSource {

	@Override
	public String getId() {
		return "Nr LoadPoles";
	}

	@Override
	public Class<?> getDataType() {
		return int.class;
	}

	@Override
	public Class<?> getSourceType() {
		return ParkingSpace.class;
	}

	@Override
	public Object get(Object obj) {
		ParkingSpace ps = (ParkingSpace) obj;
		return ps.getE();
	}

}
