package loadPoles;

import repast.simphony.context.Context;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

public class LoadPolesBuilder implements ContextBuilder<Object> {
	static final int minNormalCarPrice = 13220;

	@Override
	public Context<Object> build(Context<Object> context) {
		System.out.println("Starting Simulation");
		context.setId("loadPoles");

		// create userpanel
		UserPanel up = new UserPanel();

		// Get initializing params
		Parameters params = RunEnvironment.getInstance().getParameters();
		int humancount = params.getInteger("humancount");
		int dwellingcount = params.getInteger("dwellingcount");

		// build neighbourhood
		Neighbourhood nbh = new Neighbourhood(context, humancount, dwellingcount);
		context.add(nbh);

		// Assign b types to parkingspace.
		// standard is to distribute the parkingspaces randomly
		double bratio = params.getDouble("ratio_b");
		nbh.distributeParkingSpaceRandom(bratio);

		System.out.println("BEFORE    :" + context.getObjects(Hippie.class).size());

		return context;
	}

}
