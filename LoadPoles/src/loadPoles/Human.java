package loadPoles;

import java.util.Random;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

public class Human {
	Context<Object> context;

	int carAge; // Indicates what age a car is.
	int carAgeExpire;// the carAge in which humans change cars
	boolean carUser, hasParked;
	String carType, name;
	float happiness;
	int income;// Household incomes in the Netherlands ranges from 20.000, 64.000 which is the
				// average, to 160.000
	int initialIncome;

	// values
	double eProb, conformity, greediness, environmentalAwareness, hedonism, achievement, tradition;

	// saving parking parameters
	GridPoint location;
	public Iterable<Object> adjacent;
	public ParkingSpace parkingSpot;
	public GridCell<ParkingSpace> closestCell;

	double hippieHappiness = 0.0;
	double normalsHappiness = 0.0;

	/**
	 * Standard constructor
	 * @param context
	 */
	public Human(Context<Object> context) {
		this.context = context;

		carUser = true;
		hasParked = false;
		name = String.valueOf(context.getObjects(Human.class).size());
		happiness = 1;

		// Each car will be assigned with a random age between 0 and carAgeExpire
		carAgeExpire = RandomHelper.nextIntFromTo(8, 20);
		carAge = RandomHelper.nextIntFromTo(0, carAgeExpire);
		income = (int) Math.round((new Random().nextGaussian() * 20000 + 64000)); // mean of 64000 and standard
																					// deviation of 20000
		initialIncome = income;

		// Each human should get a random value for each of the aspects in its internal
		// value system
		environmentalAwareness = RandomHelper.nextDoubleFromTo(0, 1);
		greediness = RandomHelper.nextDoubleFromTo(0, 1);
		conformity = RandomHelper.nextDoubleFromTo(0, 1);
		hedonism = RandomHelper.nextDoubleFromTo(0, 1);
		achievement = RandomHelper.nextDoubleFromTo(0, 1);
		tradition = RandomHelper.nextDoubleFromTo(0, 1);
		eProb = 1;

		/// compute the happiness of group individuals everytime the groups change
		computeMeanHappiness(context);
	}

	/**
	 * computes the happiness of electric cars users and normal car users
	 * every time the groups change
	 * @param context
	 */
	private void computeMeanHappiness(Context<Object> context) {
		for (int i = 0; i < context.getObjects(Hippie.class).size(); i++) {
			Hippie h = (Hippie) context.getObjects(Hippie.class).get(i);
			hippieHappiness += h.getHappiness();
		}
		// average happiness of the hippies
		hippieHappiness /= context.getObjects(Hippie.class).size();

		for (int i = 0; i < context.getObjects(Normal.class).size(); i++) {
			Normal a = (Normal) context.getObjects(Normal.class).get(i);
			normalsHappiness += a.getHappiness();
		}
		// average happiness of the normals
		normalsHappiness /= context.getObjects(Normal.class).size();
	}

	/**
	 * Constructor used when a normal car user does not buy a new car
	 * @param normal
	 */
	public Human(Normal normal) {

		this.carType = null;
		this.carUser = false;
		this.name = normal.name;
		this.carAge = 0;
		this.income = normal.income;
		this.initialIncome = normal.initialIncome;
		this.carAgeExpire = 0;
		this.happiness -= normal.hedonism / 10; // He doesn't have a car so the happiness decreases

		this.environmentalAwareness = normal.environmentalAwareness;
		this.greediness = normal.greediness;
		this.conformity = normal.conformity;
		this.eProb = normal.eProb;
		this.hedonism = normal.hedonism;
		this.achievement = normal.achievement;
		this.tradition = normal.tradition;

		this.hasParked = false;
		this.location = null;
		this.adjacent = normal.adjacent;
		this.parkingSpot = null;
		this.closestCell = null;

		this.context = normal.context;

	}

	/**
	 * Constructor used when an electric car user decides not to buy a car 
	 * anymore after their former car expired
	 * @param hippie
	 */
	public Human(Hippie hippie) {

		this.carType = null;
		this.carUser = false;
		this.name = hippie.name;
		this.carAge = 0;
		this.income = hippie.income;
		this.initialIncome = hippie.initialIncome;
		this.carAgeExpire = 0;
		this.happiness -= hippie.hedonism / 10; // He doesn't have a car so the happiness decreases

		this.environmentalAwareness = hippie.environmentalAwareness;
		this.greediness = hippie.greediness;
		this.conformity = hippie.conformity;
		this.eProb = hippie.eProb;
		this.hedonism = hippie.hedonism;
		this.achievement = hippie.achievement;
		this.tradition = hippie.tradition;

		this.hasParked = false;
		this.location = null;
		this.adjacent = hippie.adjacent;
		this.parkingSpot = null;
		this.closestCell = null;

		this.context = hippie.context;

	}

	/**
	 * Tells the agent to leave with their car every day
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 2, shuffle = true)
	public void depart() {
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST HAS DEPARTED FOR TODAY ");
		}

		// Do we even use a car?
		if (!carUser || !this.hasParked) {
			return;
		}

		this.hasParked = false;

		// Get the ParkingSpace object/agent at this location and tell it we have moved
		// away.
		Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");
		try {
			for (Object o : parkingSpots.getObjectsAt(parkingSpots.getLocation(this).getX(),
					parkingSpots.getLocation(this).getY())) {
				if (o.getClass().equals(ParkingSpace.class)) {
					ParkingSpace ps = (ParkingSpace) o;
					ps.setOccupied(false);
				}
				if (o.getClass().equals(Hippie.class) || o.getClass().equals(Normal.class)) {
					context.remove(o);
					context.add(o);
				}
			}
		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
	}

	/**
	 * Tells the agent to buy a new car whenever the age of the car is the same as their expire date
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 6, shuffle = true)
	public void buyNewCar() {
		// The car age increases every 365 ticks i.e. modulo 365
		if (RunEnvironment.getInstance().getCurrentSchedule().getTickCount() % 365 == 0) {
			if (this.carUser)
				this.carAge += 1;
			// Every year electric cars do not have to pay 450$ a year which normal car do
			// have to pay for MRB
			if (this.carType == "a ")
				this.income -= 450;

			if (this.getName().equals("0")) {
				System.out.println(" THE AGENT OF INTEREST HAS A CAR OF AGE " + this.carAge);
			}
		}

		if (this.carAge == this.carAgeExpire) {
			if (this.getName().equals("0")) {
				System.out.println(" THE CAR OF THE AGENT OF INTEREST HAS EXPIRED AFTER " + this.carAge);
			}
			this.carAge = 0; // The human will buy a new car with age 0
			// We want to assign each individual in need of a new car an electric or a
			// normal car based on some parameters:
			// such as governmental policies: subsidies;
			// happiness of the person itself and the happiness of the people with electric
			// cars;
			// the financial means of the individual itself; Y

			getRidOfCar();

// -------------------------------BEGIN---------------------------------------------------------------

			if (this.income * 0.35 >= UserPanel.minEPrice) {
				if (this.getName().equals("0")) {
					System.out.println(" THE AGENT OF INTEREST with income " + this.income
							+ " IS THINKING OF BUYING AN ELECTRIC CAR of price" + UserPanel.minEPrice
							+ " and has EPROB " + this.eProb);
				}
				//Update the probability of buying an electric car
				updateEProb();

				if (RandomHelper.nextDoubleFromTo(0, 1) < this.eProb / (1 + this.eProb)) {
					buyElectricCar();
				} else {
					buyFossilCar();
					if (this.getName().equals("0")) {
						System.out.println("THE AGENT OF INTEREST has bought a FOSSIL FUELED car " + this.eProb / (1 + this.eProb));
					}
				}
			} else {
				if (this.income * 0.35 >= LoadPolesBuilder.minNormalCarPrice) {
					buyFossilCar();
					if (this.getName().equals("0")) {
						System.out.println("THE AGENT OF INTEREST has bought a FOSSIL FUELED car " + this.income * 0.35
								+ " < " + UserPanel.minEPrice);
					}
				} else {
					// If the agent does not have enough money i.e. 35% of its income is not higher
					// than the car price
					// don't buy a car

					if (this.getName().equals("0")) {
						System.out.println("THE AGENT OF INTEREST has not BOUGHT A CAR " + this.income * 0.35 + " < "
								+ LoadPolesBuilder.minNormalCarPrice);
					}

					buyNoNewCar();

				}
			}
			//creating new instances according to the car that has been bought
			if (this.carType == "b" && this.getClass() == Normal.class) {
				Hippie h = new Hippie(this.context, (Normal) this);
			}
			if (this.carType == "a" && this.getClass() == Hippie.class) {
				Normal a = new Normal(this.context, (Hippie) this);
			}
			if (this.carType == "a" && this.getClass() == Human.class) {
				Normal h = new Normal(this.context, this);
			}
			if (this.carType == "b" && this.getClass() == Human.class) {
				Hippie h = new Hippie(this.context, this);
			}
		}

	}

	/**
	 * Buy no new car after expiration date since the agent did not have enough 
	 * money to buy it
	 */
	private void buyNoNewCar() {
		this.carType = null;
		this.carAge = 0;
		this.carAgeExpire = 0;
		this.carUser = false;

		if (this.getClass().equals(Hippie.class)) {
			Hippie h = (Hippie) this;
			Human noncaruser = new Human(h);
			context.add(noncaruser);
			context.remove(h);
		}
		if (this.getClass().equals(Normal.class)) {
			Normal n = (Normal) this;
			Human noncaruser = new Human(n);
			context.add(noncaruser);
			context.remove(n);
		}
	}

	/**
	 * Buy a fossil car after expiration date since the agent did not have enough money
	 * to buy an electric car or the probability to buy onw was too low
	 */
	private void buyFossilCar() {
		this.carType = "a";
		// BPM tax which doesn't need to be payed by electric cars (eenmalige belasting)
		this.income -= 3957 + LoadPolesBuilder.minNormalCarPrice;
		if (this.getClass() == Normal.class) {
			// If a normal scraps its car it gets a 10000$ scrap money
			this.income += 1000;
		}
		// Increase the happiness when buying a new car
		this.happiness += ((this.hedonism * UserPanel.hedonism) - (this.greediness * UserPanel.greediness))
				/ 10;
	}

	/**
	 * Buy an electric car 
	 */
	private void buyElectricCar() {
		this.carType = "b"; // Buy a b car whether you're an a or a b
		// System.out.println("eProb op moment van koop: " + eProb);
		// In 2021 you will get wat is in de slider when buying an electric car
		// You currently get 27% tax deductability when buying an electric car
		this.income += UserPanel.subsidyValue + (0.27 - 1) * UserPanel.minEPrice;
		// increase the happiness when buying a new car
		this.happiness += ((this.hedonism * UserPanel.hedonism) - (this.greediness * UserPanel.greediness))
				/ 10;
		if (this.getName().equals("0")) {
			System.out.println("THE AGENT OF INTEREST has bought an EV " + this.eProb / (1 + this.eProb));
		}
	}

	/**
	 * Updates the probability of buying an electric car given the Schwartz values
	 * happiness
	 * governmental subsidies
	 */
	private void updateEProb() {
		// TRADITION--------------------------
		// If an agent already has an electric car the chance he will again buy an
		// electric car will go up by factor 10 of the value: tradition
		if (this.carType == "b") {
			this.eProb += this.eProb * ((this.tradition * UserPanel.tradition) / 10);
			if (this.getName().equals("0")) {
				System.out.println(" EPROB OF THE AGENT OF INTEREST WITH EV increased by (tradition) "
						+ this.eProb * ((this.tradition * UserPanel.tradition) / 10));
			}
		}

		else {
			this.eProb -= this.eProb * ((this.tradition * UserPanel.tradition) / 10);
			if (this.getName().equals("0")) {
				System.out.println(" EPROB OF THE AGENT OF INTEREST WITHOUT EV decreased by (tradition) "
						+ this.eProb * ((this.tradition * UserPanel.tradition) / 10));
			}
		}
		// END: TRADITION---------------------

		// ------------------------incentives, environment and greediness
		// ---------------------
		this.eProb += this.eProb * (this.environmentalAwareness * UserPanel.environmentalAwareness) / 10;
		this.eProb -= this.eProb * (this.greediness * UserPanel.greediness) / 10;
		this.eProb += UserPanel.subsidyValue * 0.0001;
		if (this.getName().equals("0")) {
			System.out.println(" EPROB OF THE AGENT OF INTEREST increased by (envAwareness) "
					+ this.eProb * (this.environmentalAwareness * UserPanel.environmentalAwareness) / 10);
			System.out.println(" EPROB OF THE AGENT OF INTEREST decreased by (greediness) "
					+ this.eProb * (this.greediness * UserPanel.greediness) / 10);
			System.out.println(" EPROB OF THE AGENT OF INTEREST increased by (subsidy) "
					+ UserPanel.subsidyValue * 0.0001);
		}
		// ------------------------END: incentives, environment and greediness
		// ----------------

		// ---------------------------Happiness and conformity
		// --------------------------------
		// the average happiness of electric cars compared to normal cars influences
		// eProb
		// and the average happiness compared to their own happiness also influence
		// eProb
		if (hippieHappiness > normalsHappiness) {
			this.eProb += this.eProb * (this.conformity * UserPanel.conformity) / 10;
			if (this.getName().equals("0")) {
				System.out
						.println(" EPROB OF THE AGENT OF INTEREST increased by (EVhappiness > NormalHappiness) "
								+ this.eProb * (this.conformity * UserPanel.conformity) / 10);
			}
		} else {
			this.eProb -= this.eProb * (this.conformity * UserPanel.conformity) / 10;
			if (this.getName().equals("0")) {
				System.out
						.println(" EPROB OF THE AGENT OF INTEREST decreased by (EVhappiness < NormalHappiness) "
								+ this.eProb * (this.conformity * UserPanel.conformity) / 10);
			}
		}

		if (hippieHappiness > this.happiness) {
			this.eProb += this.eProb * (this.conformity * UserPanel.conformity) / 10;
			if (this.getName().equals("0")) {
				System.out
						.println(" EPROB OF THE AGENT OF INTEREST increased by (EVhappiness > agentHappiness) "
								+ this.eProb * (this.conformity * UserPanel.conformity) / 10);
			}
		} else {
			this.eProb -= this.eProb * (this.conformity * UserPanel.conformity) / 10;
			if (this.getName().equals("0")) {
				System.out
						.println(" EPROB OF THE AGENT OF INTEREST decreased by (EVhappiness > agentHappiness) "
								+ this.eProb * (this.conformity * UserPanel.conformity) / 10);
			}
		}
		// ---------------------------END: Happiness and conformity
		// ---------------------------

		// -------------------income-------------------------------------------------------
		if (this.income < 52000) {
			this.eProb -= 1 - (this.income / 52000);
			if (this.getName().equals("0")) {
				System.out.println(" EPROB OF THE AGENT OF INTEREST decreased by (income < 52000) "
						+ (1 - (this.income / 52000)));
			}
		} else {
			this.eProb += 1 - (52000 / this.income);
			if (this.getName().equals("0")) {
				System.out.println(" EPROB OF THE AGENT OF INTEREST increased by (income > 52000) "
						+ (1 - (52000 / this.income)));
			}
		}
		// -------------------END:income--------------------------------------------------
	}

	/**
	 * Before buying a new car, the car has to disappear from the neighbourhood grid
	 */
	private void getRidOfCar() {
		if (location != null) {
			Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");
			for (Object o : parkingSpots.getObjectsAt(this.location.getX(), this.location.getY())) {
				if (o.getClass().equals(ParkingSpace.class)) {
					ParkingSpace ps = (ParkingSpace) o;
					ps.setOccupied(false);
				}
			}
		}
	}
	
	/**
	 * The income of each individual will increase every 30 days
	 */
	@ScheduledMethod(start = 1, interval = 30, priority = 7, shuffle = true)
	public void increaseIncome() {
		this.income += (this.initialIncome / 12) * UserPanel.savings;
	}

	
	
	//////////Getters
	// Return the happiness
	public double getHappiness() {
		return this.happiness;
	}

	// is parked?
	public boolean getHasParked() {
		return this.hasParked;
	}

	public String getName() {
		return this.name;
	}

	public String getType() {
		return this.carType;
	}

	// Indicates the age of a car
	public int getcarAge() {
		return this.carAge;
	}

	public int getCarExpire() {
		return this.carAgeExpire;
	}

	// Indicates the age of a car
	public int getincome() {
		return this.income;
	}

	public double getEnvirA() {
		if (UserPanel.environmentalAwareness == 0)
			return (this.environmentalAwareness * UserPanel.environmentalAwareness + 1);
		return this.environmentalAwareness * UserPanel.environmentalAwareness;
	}

	// Indicates the hedonism of the human
	public double getHedonism() {
		if (UserPanel.hedonism == 0)
			return (this.hedonism * UserPanel.hedonism + 1);
		return (this.hedonism * UserPanel.hedonism);
	}

	// Indicates the Achievement of the human
	public double getAchievement() {
		return (this.achievement * UserPanel.achievement);
	}

	public double getConformity() {
		return this.conformity;
	}

	public double getTradition() {
		return this.tradition;
	}

	public double getGreediness() {
		return this.greediness;
	}

	public GridPoint getPSLocation() {
		Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");
		return parkingSpots.getLocation(this);
	}

}
