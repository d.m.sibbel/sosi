package loadPoles;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Iterator;

import repast.simphony.context.Context;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.context.space.grid.GridFactory;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.SimpleGridAdder;
import repast.simphony.space.grid.StrictBorders;
import repast.simphony.util.collections.IndexedIterable;

public class Neighbourhood {

	Context<Object> context;
	Grid<Object> dwellingsGrid, parkingspacesGrid;
	int gridX = 12, gridY = 12;
	int minAmountOfHippiesToBuild = UserPanel.CSBuildRate;
	double pastH = 0;
	double curH = 0;
	double oldH = 0;
	public static int nCS = 0;

	public static double pollution = 15.23;

	static ArrayList<ParkingSpace> bSpots = new ArrayList<ParkingSpace>();
	static ArrayList<ParkingSpace> aSpots = new ArrayList<ParkingSpace>();

	/** 
	 * Standard constructor of a neighbourhood 
	 * @param context
	 */
	public Neighbourhood(Context<Object> context) {
		this(context, 10, 10);
		System.out.println("creating neighbourhood");
	}

	public Neighbourhood(Context<Object> context, int humancount, int dwellingcount) {
		System.out.println("- Creating neighbourhood!");
		this.context = context;

		GridFactory factory = GridFactoryFinder.createGridFactory(null);

		// Has dwellings
		dwellingsGrid = factory.createGrid("dwellingsGrid", context, new GridBuilderParameters<Object>(
				new StrictBorders(), new RandomGridAdder<Object>(), true, gridX, gridY));

		// Has parking spaces
		parkingspacesGrid = factory.createGrid("parkingspacesGrid", context, new GridBuilderParameters<Object>(
				new StrictBorders(), new SimpleGridAdder<Object>(), true, gridX, gridY));

		// Add dwellings
		for (int i = 0; i < dwellingcount; i++) {
			context.add(new Dwelling(context, dwellingsGrid));
		}

		// Add people
		Parameters params = RunEnvironment.getInstance().getParameters();
		for (int i = 0; i < humancount; i++) {
			if (RandomHelper.nextDoubleFromTo(0, 1) < params.getDouble("ratioElectric")) {
				context.add(new Hippie(context));
			} else {
				context.add(new Normal(context));
			}
		}

		// make sure they have a home
		NetworkBuilder<Object> netBuilder = new NetworkBuilder<Object>("livingin", context, true);
		Network<Object> livingin = netBuilder.buildNetwork();

		IndexedIterable<Object> humans = context.getObjects(Human.class);
		Iterator<Object> humansIterator = humans.iterator();
		IndexedIterable<Object> dwellings = context.getObjects(Dwelling.class);

		// And make someone live somewhere by adding an edge in the network.
		while (humansIterator.hasNext()) {
			Object x = dwellings.get(RandomHelper.nextIntFromTo(0, dwellings.size() - 1));
			Human h = (Human) humansIterator.next();
			livingin.addEdge(h, x);
			h.adjacent = livingin.getAdjacent(h);
		}

		// fill the parkingspaceGrid with parkingspaces by adding to context and moving
		// them to a location
		int parkX = parkingspacesGrid.getDimensions().getWidth();
		int parkY = parkingspacesGrid.getDimensions().getHeight();
		for (int i_x = 0; i_x < parkX; i_x++) {
			for (int i_y = 0; i_y < parkY; i_y++) {
				ParkingSpace ps = new ParkingSpace(context);
				context.add(ps);
				parkingspacesGrid.moveTo(ps, i_x, i_y);
			}
		}

	}

	// Distribute B type parking spaces at random given a ratio of B to A.
	public void distributeParkingSpaceRandom(double ratio) {
		Iterable<Object> parkingSpacesGrid = this.parkingspacesGrid.getObjects();
		Iterator<Object> parkingSpacesGridIterator = parkingSpacesGrid.iterator();
		while (parkingSpacesGridIterator.hasNext()) {
			try {
				ParkingSpace ps = (ParkingSpace) parkingSpacesGridIterator.next();

				// Do it randomly
				// ratio% distribution of b compared to a
				if (RandomHelper.nextDoubleFromTo(0, 1) < ratio) {
					ps.setType("b");
					bSpots.add(ps);
				} else {
					ps.setType("a");
					aSpots.add(ps);
				}

			} catch (Exception e) {
				// System.out.println("Exception: " + e);
			} finally {
				// do nothing
			}
		}
	}

	/**
	 * Calculate the pollution at each tick depending on the ratio normal cars vs.
	 * electric cars
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 4)
	public void CalculatePollution() {

		double h = context.getObjects(Hippie.class).size();
		pollution += (oldH - h) / 50;
		oldH = h;

	}

	/**
	 * Created charging stations. These are meant to be charging stations for
	 * example, at work. With more of these, electric cars can be charged not only
	 * in their neighborhood but also on the go
	 */
	@ScheduledMethod(start = 30, interval = 30, priority = 5)
	public void MakeChargingStations() {
		int curH = context.getObjects(Hippie.class).size();
		if (curH != pastH) {
			if (curH > minAmountOfHippiesToBuild) {
				nCS++;
				System.out.println("A charging Station has been added");
				minAmountOfHippiesToBuild = curH + UserPanel.CSBuildRate;
			}
		}
		pastH = curH;
	}

	/**
	 * Creates every month new charging pole parking spots if new some amount of
	 * electric cars are bought (poleBuildRate)
	 */
	@ScheduledMethod(start = 30, interval = 30, priority = 5)
	public void FromAtoBParkingSpots() {
		int h = context.getObjects(Hippie.class).size();
		int a = context.getObjects(Normal.class).size();

		if (bSpots.size() < h - UserPanel.poleBuildRate) {
			int r = RandomHelper.nextIntFromTo(0, aSpots.size() - 1);
			ParkingSpace aSpot = aSpots.get(r);
			if (aSpot.type != "b") {
				aSpot.setType("b");
				aSpots.remove(aSpot);
				bSpots.add(aSpot);
				System.out.println(" Just added another electric pole parkingspace");
			}
		}
		if (aSpots.size() < a - UserPanel.poleBuildRate) {
			int r = RandomHelper.nextIntFromTo(0, bSpots.size() - 1);
			ParkingSpace bspot = bSpots.get(r);
			if (bspot.type != "a") {
				bspot.setType("a");
				bSpots.remove(bspot);
				aSpots.add(bspot);
				System.out.println(" Just added another normal car spot");
			}
		}
	}

	@ScheduledMethod(start = 365, interval = 365, priority = 6)
	public void newYear() {
		System.out.println(RunEnvironment.getInstance().getCurrentSchedule().getTickCount()
				+ "-----------------------------------------------------------"
				+ RunEnvironment.getInstance().getCurrentSchedule().getTickCount() / 365);

		if (RunEnvironment.getInstance().getCurrentSchedule().getTickCount() == 3650) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("AFTER 10y    :" + context.getObjects(Hippie.class).size());
			// RunEnvironment.getInstance().endRun();
		}
	}

	// Indicates the number of CS of the environment
	public double getnCS() {
		return nCS;
	}

	// Indicates the pollution of the environment
	public double getPollution() {
		return pollution;
	}
}
