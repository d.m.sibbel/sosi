package loadPoles;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import repast.simphony.ui.RSApplication;

public class UserPanel implements ActionListener {
	static int poleBuildRate = 5;
	static int CSBuildRate = 5;
	static int minEPrice = 22490;
	static int subsidyValue = 5000;
	static double savings = 0.1;
	static double conformity = 1, greediness = 1, environmentalAwareness = 1, hedonism = 1, achievement = 1,
			tradition = 1;

	boolean conformityOff, greedinessOff, environmentalAwarenessOff, hedonismOff, achievementOff, traditionOff;
	boolean savingsOff;

	UserPanel() {

		JPanel up = new JPanel();
		up.setLayout(new BoxLayout(up, BoxLayout.PAGE_AXIS));
		addPolesSlider(up);
		addCSSlider(up);
		addminEPrice(up);
		addSubsidySlider(up);
		addSchwartz(up);
		addNeighbourhoodStatus(up);
		RSApplication.getRSApplicationInstance().addCustomUserPanel(up);

	}

	private void addPolesSlider(JPanel up) {
		JLabel label = new JLabel("BuildRate LoadPoles");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(100);
		slider.setValue(poleBuildRate);
		slider.setMajorTickSpacing(10);
		slider.setPaintTicks(true);

		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(0, new JLabel("fast"));
		labelTable.put(100, new JLabel("slow"));
		slider.setLabelTable(labelTable);
		slider.setPaintLabels(true);
		slider.repaint();
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				System.out.println("loadPoles    :" + slider.getValue());
				poleBuildRate = slider.getValue();
			}

		});
		up.add(slider);
	}

	private void addCSSlider(JPanel up) {
		JLabel label = new JLabel("BuildRate CS");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(50);
		slider.setMajorTickSpacing(10);
		slider.setValue(CSBuildRate);
		slider.setPaintTicks(true);

		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(0, new JLabel("fast"));
		labelTable.put(50, new JLabel("slow"));
		slider.setLabelTable(labelTable);
		slider.setPaintLabels(true);
		slider.repaint();
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				System.out.println("CSs    :" + slider.getValue());
				CSBuildRate = slider.getValue();
			}

		});
		up.add(slider);
	}

	private void addminEPrice(JPanel up) {
		int minmin = 18000;
		int minmax = 80000;

		JLabel label = new JLabel("eCar min. price");
		up.add(label);

		JSlider slider = new JSlider();
		slider.setMinimum(minmin);
		slider.setMaximum(minmax);
		slider.setValue(minEPrice);
		slider.setMajorTickSpacing(1000);
		slider.setPaintTicks(true);

		Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
		labelTable.put(minmin, new JLabel(String.valueOf(minmin)));
		labelTable.put(minmax, new JLabel(String.valueOf(minmax)));
		slider.setLabelTable(labelTable);
		slider.setPaintLabels(true);
		slider.repaint();

		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				System.out.println("min eCar price    :" + slider.getValue());
				minEPrice = slider.getValue();
			}

		});
		up.add(slider);
	}

	private void addSubsidySlider(JPanel up) {

		JLabel label = new JLabel("Government subsidies");
		up.add(label);
		JSlider slider = new JSlider();
		slider.setMinimum(0);
		slider.setMaximum(10000);
		slider.setValue(subsidyValue);
		slider.setPaintTicks(true);
		slider.setMajorTickSpacing(2000);
		slider.setPaintLabels(true);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				subsidyValue = slider.getValue();
			}

		});
		up.add(slider);
	}

	private void addSchwartz(JPanel up) {
		JLabel label = new JLabel("Schwartz values");
		up.add(label);

		JPanel t = new JPanel();
		t.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		JButton conformity = new JButton("conformity");
		conformity.setPreferredSize(new Dimension(100, 50));
		conformity.addActionListener(this);
		conformity.setForeground(Color.BLACK);
		c.gridx = 0;
		c.gridy = 0;
		t.add(conformity, c);

		JButton greediness = new JButton("greediness");
		greediness.setPreferredSize(new Dimension(100, 50));
		greediness.addActionListener(this);
		greediness.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 0;
		t.add(greediness, c);

		JButton environmentalAwareness = new JButton("environmental");
		environmentalAwareness.setPreferredSize(new Dimension(100, 50));
		environmentalAwareness.addActionListener(this);
		environmentalAwareness.setForeground(Color.BLACK);
		c.gridx = 2;
		c.gridy = 0;
		t.add(environmentalAwareness, c);

		JButton hedonism = new JButton("hedonism");
		hedonism.setPreferredSize(new Dimension(100, 50));
		hedonism.addActionListener(this);
		hedonism.setForeground(Color.BLACK);
		c.gridx = 0;
		c.gridy = 1;
		t.add(hedonism, c);

		JButton achievement = new JButton("achievement");
		achievement.setPreferredSize(new Dimension(100, 50));
		achievement.addActionListener(this);
		achievement.setForeground(Color.BLACK);
		c.gridx = 1;
		c.gridy = 1;
		t.add(achievement, c);

		JButton tradition = new JButton("tradition");
		tradition.setPreferredSize(new Dimension(100, 50));
		tradition.addActionListener(this);
		tradition.setForeground(Color.BLACK);
		c.gridx = 2;
		c.gridy = 1;
		t.add(tradition, c);

		t.setPreferredSize(new Dimension(200, 3 * 50));
		up.add(t);
	}

	private void addNeighbourhoodStatus(JPanel up) {
		JLabel label = new JLabel("Neighbourhood");
		up.add(label);

		JPanel t = new JPanel();
		t.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		JButton neighbourhood = new JButton("neighbourhood");
		neighbourhood.setPreferredSize(new Dimension(100, 50));
		neighbourhood.setForeground(Color.BLACK);
		neighbourhood.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (savingsOff) {
					savings = 0.1;
					neighbourhood.setText("normal");
					savingsOff = false;
					neighbourhood.setForeground(Color.BLACK);
					neighbourhood.repaint();
				} else {
					savings = 0.3;
					neighbourhood.setText("rich");
					savingsOff = true;
					neighbourhood.setForeground(Color.GRAY);
					neighbourhood.repaint();
				}
				System.out.println(savings);
			}

		});
		c.gridx = 0;
		c.gridy = 0;

		t.add(neighbourhood, c);

		t.setPreferredSize(new Dimension(200, 3 * 50));
		up.add(t);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		JButton button = (JButton) e.getSource();
		switch (e.getActionCommand()) {
		case "conformity":

			if (conformityOff) {
				conformity = 1;
				conformityOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				conformity = 0;
				conformityOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		case "greediness":
			if (greedinessOff) {
				greediness = 1;
				greedinessOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				greediness = 0;
				greedinessOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		case "environmental":
			if (environmentalAwarenessOff) {
				environmentalAwareness = 1;
				environmentalAwarenessOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				environmentalAwareness = 0;
				environmentalAwarenessOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		case "hedonism":
			if (hedonismOff) {
				hedonism = 1;
				hedonismOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				hedonism = 0;
				hedonismOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		case "achievement":
			if (achievementOff) {
				achievement = 1;
				achievementOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				achievement = 0;
				achievementOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		case "tradition":
			if (traditionOff) {
				tradition = 1;
				traditionOff = false;
				button.setForeground(Color.BLACK);
				button.repaint();
			} else {
				tradition = 0;
				traditionOff = true;
				button.setForeground(Color.GRAY);
				button.repaint();
			}
			break;
		default:
			break;
		}
		System.out.println(conformity + " " + greediness + " " + environmentalAwareness + " " + hedonism + " "
				+ achievement + " " + tradition);
	}
}
