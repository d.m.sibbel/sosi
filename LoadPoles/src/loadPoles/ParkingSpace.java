package loadPoles;

import repast.simphony.context.Context;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;

public class ParkingSpace {

	Context<Object> context;
	String type;
	boolean occupied;

	public ParkingSpace(Context<Object> context) {
		this.context = context;
		this.type = "a";
		this.occupied = false;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean getOccupied() {
		return this.occupied;
	}

	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}

	public GridPoint getLocation() {
		Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");
		return parkingSpots.getLocation(this);
	}

	public Object getE() {
		if (this.getType() == "a")
			return 1;
		if (this.getType() == "b")
			return 0;
		return -1;
	}
}
// 