package loadPoles;

import java.util.ArrayList;
import java.util.List;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.query.space.grid.GridCellNgh;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.SimUtilities;

public class Hippie extends Human {

	boolean needsCharging;

	/**
	 * Standard constructor of an electric car user
	 * @param context
	 */
	public Hippie(Context<Object> context) {
		super(context);
		this.carType = "b";
		if (RandomHelper.nextDoubleFromTo(0, 1) > 0.5) {
			needsCharging = false;
		} else {
			needsCharging = true;
		}
		System.out.println("A new electric car user has been created: " + this.getName());
	}

	/**
	 * Constructor used when fossil car user buys an electric car
	 * @param context
	 * @param normal
	 */
	public Hippie(Context<Object> context, Normal normal) {
		super(context);
		this.needsCharging = true;

		this.carType = "b";
		this.carUser = normal.carUser;
		this.name = normal.name;
		this.carAge = normal.carAge;
		this.income = normal.income;
		this.initialIncome = normal.initialIncome;
		this.happiness = normal.happiness;
		this.carAgeExpire = normal.carAgeExpire;

		this.environmentalAwareness = normal.environmentalAwareness;
		this.greediness = normal.greediness;
		this.conformity = normal.conformity;
		this.eProb = normal.eProb;
		this.hedonism = normal.hedonism;
		this.achievement = normal.achievement;
		this.tradition = normal.tradition;

		this.hasParked = normal.hasParked;
		this.location = normal.location;
		this.adjacent = normal.adjacent;
		this.parkingSpot = normal.parkingSpot;
		this.closestCell = normal.closestCell;

		context.add(this);
		context.remove(normal);

		System.out.println("Human has changed their normal car for an electric one "+ this.getName());
	}

	/**
	 * Constructor used when a human (without a car) buys an electric car
	 * @param context
	 * @param human
	 */
	public Hippie(Context<Object> context, Human human) {
		super(context);
		this.needsCharging = true;

		this.carType = "b";
		this.carUser = true;
		this.name = human.name;
		this.carAge = human.carAge;
		this.income = human.income;
		this.initialIncome = human.initialIncome;
		this.happiness = 1;
		this.carAgeExpire = RandomHelper.nextIntFromTo(8, 20);

		this.environmentalAwareness = human.environmentalAwareness;
		this.greediness = human.greediness;
		this.conformity = human.conformity;
		this.eProb = human.eProb;
		this.hedonism = human.hedonism;
		this.achievement = human.achievement;
		this.tradition = human.tradition;

		this.hasParked = false;
		this.location = human.location;
		this.adjacent = human.adjacent;
		this.parkingSpot = human.parkingSpot;
		this.closestCell = human.closestCell;

		context.add(this);
		context.remove(human);
		
		System.out.println("Human has bought an electric one "+ this.getName());
	}

	/**
	 * Tells all electric car users to park their car daily
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 1, shuffle = true)
	public void parkCar() {
		// do we even use a car?
		if (!carUser)
			return;

		// Find best empty spot
		// Get location of home
		Grid<Object> grid = (Grid<Object>) context.getProjection("dwellingsGrid");

		Iterable<Object> home = this.adjacent;
		GridPoint homeLocation = grid.getLocation(home.iterator().next());

		// Now look for parking spots
		Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");

		// get all cells around the home location
		GridCellNgh<ParkingSpace> nghCreator = new GridCellNgh<ParkingSpace>(parkingSpots, homeLocation,
				ParkingSpace.class, 100, 100);
		List<GridCell<ParkingSpace>> gridCells = nghCreator.getNeighborhood(true);
		SimUtilities.shuffle(gridCells, RandomHelper.getUniform());

		// Check the neighbourhood for the closest empty spot
		// The spot has either no agent in it or an agent that isn't parked
		double minDistance = Double.MAX_VALUE, parkDistance = 0;
		GridCell<ParkingSpace> closestCell = null;
		ParkingSpace closestSpot = null;

		boolean chargingAndB = false, noChargingAndA = false, chargingAndA = false, noChargingAndB = false;
		for (GridCell<ParkingSpace> cell : gridCells) {

			ParkingSpace spot = (ParkingSpace) parkingSpots.getObjectAt(cell.getPoint().getX(), cell.getPoint().getY());

			if (needsCharging) {
				if (!parkingFull(Neighbourhood.bSpots)) { // If there are empty b spots park in the closest one
					if (!spot.getOccupied() && spot.getType() == "b") {
						parkDistance = parkingSpots.getDistance(homeLocation, cell.getPoint());
						if (parkDistance < minDistance) {
							minDistance = parkDistance;
							closestCell = cell;
							closestSpot = spot;
							chargingAndB = true;
						}
					}
				} else { // if there are no b parking spots park in the closest a spot
					if (!spot.getOccupied() && spot.getType() == "a") {
						// park in a space with a loadPole
						parkDistance = parkingSpots.getDistance(homeLocation, cell.getPoint());
						if (parkDistance < minDistance) {
							minDistance = parkDistance;
							closestCell = cell;
							closestSpot = spot;
							chargingAndA = true;
						}
					}
				}
			} else {
				if (!parkingFull(Neighbourhood.aSpots)) { // If there are empty a spots park in the closest one
					if (!spot.getOccupied() && spot.getType() == "a") {
						parkDistance = parkingSpots.getDistance(homeLocation, cell.getPoint());
						if (parkDistance < minDistance) {
							minDistance = parkDistance;
							closestCell = cell;
							closestSpot = spot;
							noChargingAndA = true;
						}
					}
				} else { // if there are no a parking spots park in the closest b spot
					if (!spot.getOccupied() && spot.getType() == "b") {
						// park in a space with a loadPole
						parkDistance = parkingSpots.getDistance(homeLocation, cell.getPoint());
						if (parkDistance < minDistance) {
							minDistance = parkDistance;
							closestCell = cell;
							closestSpot = spot;
							noChargingAndB = true;
						}
					}
				}
			}
		}

		// If we have found a parking spot: park!
		if (closestCell != null) {
			parkOnSpot(parkingSpots, parkDistance, closestCell, closestSpot, chargingAndB, noChargingAndA, chargingAndA,
					noChargingAndB);
		} else {
			decreaseParkingHappiness();
		}
		//
		if (this.happiness > 1)
			this.happiness = 1;
		if (this.happiness < -1)
			this.happiness = -1;
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST HAS HAPPINESS " + this.happiness);
		}
	}

	/**
	 * decreases the happiness of an agent that was not able to park
	 */
	private void decreaseParkingHappiness() {
		if (this.happiness >= -1)
			this.happiness += (float) Neighbourhood.nCS / 1000 - this.getAchievement() / 100
					- this.getHedonism() / 100 + this.getEnvirA() * (15.23 - Neighbourhood.pollution)/10;
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST WAS NOT ABLE TO FIND A SPOT :-(. Happines increased by "
					+ (Neighbourhood.nCS / 1000 - this.getAchievement() / 100 - this.getHedonism() / 100));
		}
	}

	/**
	 * The car is parked on the closest spot to home
	 * @param parkingSpots
	 * @param parkDistance
	 * @param closestCell
	 * @param closestSpot
	 * @param chargingAndB
	 * @param noChargingAndA
	 * @param chargingAndA
	 * @param noChargingAndB
	 */
	private void parkOnSpot(Grid<Object> parkingSpots, double parkDistance, GridCell<ParkingSpace> closestCell,
			ParkingSpace closestSpot, boolean chargingAndB, boolean noChargingAndA, boolean chargingAndA,
			boolean noChargingAndB) {
		parkingSpots.moveTo(this, closestCell.getPoint().getX(), closestCell.getPoint().getY());
		this.location = parkingSpots.getLocation(this);
		closestSpot.setOccupied(true);
		this.parkingSpot = closestSpot;
		this.closestCell = closestCell;
		this.hasParked = true;
		if (noChargingAndB) {
			if (this.getName().equals("0")) {
				System.out.println(" THE AGENT OF INTEREST neededNOT CHARGING AND PARKED ON B :-( ");
			}
		}
		if (chargingAndA) {
			if (this.getName().equals("0")) {
				System.out.println(" THE AGENT OF INTEREST needed CHARGING AND PARKED ON A :-( ");
			}
		}
		if (chargingAndB) {
			if (this.getName().equals("0")) {
				System.out.println(" THE AGENT OF INTEREST needed CHARGING AND PARKED ON B :-)");
			}
		}
		if (noChargingAndA) {
			if (this.getName().equals("0")) {
				System.out.println(" THE AGENT OF INTEREST neededNOT CHARGING AND PARKED ON A :-) ");
			}
		}

		updateHappiness(parkDistance, closestSpot);
	}

	/**
	 * Updates the happiness of an agent depending on the type of parking space they parked on and 
	 * whether the car needed charging or not
	 * @param parkDistance
	 * @param closestSpot
	 */
	private void updateHappiness(double parkDistance, ParkingSpace closestSpot) {
		if ((closestSpot.getType() == "b" && !needsCharging) || (closestSpot.getType() == "a" && needsCharging)) {
			// If a car needs charging CS will increase its happiness seeing as it has that
			// as an available alternative
			if (this.happiness <= 1 && this.happiness >= -1) {
				this.happiness += (float) this.getEnvirA() * (15.23 - Neighbourhood.pollution)/10
						- this.getHedonism() / 1000 * parkDistance * parkDistance - this.getAchievement() / 10;

			}
			if (this.getName().equals("0")) {
				System.out.println(" THE HAPINESS OF THE AGENT OF INTEREST HAS increased BY "
						+ (-this.getHedonism() / 1000 * parkDistance * parkDistance - this.getAchievement() / 10));
				System.out.println(" Park distance: " + parkDistance);
			}
		}
		if ((closestSpot.getType() == "b" && needsCharging) || (closestSpot.getType() == "a" && !needsCharging)) {
			// the closest spot is of type a
			this.happiness += (float) this.getEnvirA() * (15.23 - Neighbourhood.pollution)/10
					- this.getHedonism() / 1000 * parkDistance * parkDistance + this.getAchievement() / 10;
			if (this.getName().equals("0")) {
				System.out.println(" THE HAPINESS OF THE AGENT OF INTEREST HAS increased BY "
						+ (this.getEnvirA() * (15.23 - Neighbourhood.pollution)/10
								- this.getHedonism() / 1000 * parkDistance * parkDistance
								+ this.getAchievement() / 10));
				System.out.println(" Park distance: " + parkDistance);
			}
		}
	}

	/**
	 * Tells all agents to charge their car if they are parked on a parking space with a load pole
	 * other wise they are not able to park
	 * They can also park outside the neighbourhood on a charging station 
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 3, shuffle = true)
	public void charge() {
		// If a car is parked and it needed charging before parking when 1 tick i.e. 1
		// day has passed the car is charged and should therefore move on
		if (this.parkingSpot != null) {
			if (this.parkingSpot.getType() == "b") {
				if (this.needsCharging) {
					this.needsCharging = false;
					if (this.getName().equals("0")) {
						System.out.println(" THE AGENT OF INTEREST HAS CHARGED ");
					}
				}
			} else {
				this.needsCharging = true;
				if (this.getName().equals("0")) {
					System.out.println(" THE AGENT OF INTEREST WAS NOT ABLE TO CHARGE ");
				}
			}
			// If the electric car needs charging but wasn't able to park in a "b" spot
			// then depending on the amount of CS the car could with probability nCS/100
			// Charge on the way to work or at work/ tanking station and thereby return home
			// fully charged
			if (this.needsCharging && this.parkingSpot.getType() == "a") {
				if (RandomHelper.nextDoubleFromTo(0, 1) < Neighbourhood.nCS / 100) {
					this.needsCharging = false;
					if (this.getName().equals("0")) {
						System.out.println(" THE AGENT OF INTEREST HAS CHARGED ITS CAR ON THE GO ");
					}
				}
			}
		}

	}

	/**
	 * Tests whether the parking spaces are full
	 * @param parkingSpots
	 * @return
	 */
	private boolean parkingFull(ArrayList<ParkingSpace> parkingSpots) {
		for (ParkingSpace x : parkingSpots) {
			if (!x.getOccupied())
				return false;
		}
		return true;
	}

	/**
	 * method used by the UI to get the number of electric car users
	 * @return
	 */
	public int getHuman() {
		return context.getObjects(Hippie.class).size();
	}
	
	public boolean getNeedsCharging() {
		return this.needsCharging;
	}

}
