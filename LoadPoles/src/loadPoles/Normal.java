package loadPoles;

import java.util.List;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.query.space.grid.GridCell;
import repast.simphony.query.space.grid.GridCellNgh;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.graph.Network;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.SimUtilities;

public class Normal extends Human {
	
	/**
	 * Standard constructor
	 * @param context
	 */
	public Normal(Context<Object> context) {
		super(context);
		this.carType = "a";
		System.out.println("A new normal car user has been created: " + this.getName());
	}

	/**
	 * Constructor used when an electric car user switch to a fossil fueled car
	 * @param context
	 * @param hippie
	 */
	public Normal(Context<Object> context, Hippie hippie) {
		super(context);
		this.context = hippie.context;

		this.carType = "a";
		this.carUser = hippie.carUser;
		this.name = hippie.name;
		this.carAge = hippie.carAge;
		this.income = hippie.income;
		this.initialIncome = hippie.initialIncome;
		this.happiness = hippie.happiness;
		this.carAgeExpire = hippie.carAgeExpire;

		this.environmentalAwareness = hippie.environmentalAwareness;
		this.greediness = hippie.greediness;
		this.conformity = hippie.conformity;
		this.eProb = hippie.eProb;
		this.hedonism = hippie.hedonism;
		this.achievement = hippie.achievement;
		this.tradition = hippie.tradition;

		this.hasParked = hippie.hasParked;
		this.location = hippie.location;
		this.adjacent = hippie.adjacent;
		this.parkingSpot = hippie.parkingSpot;
		this.closestCell = hippie.closestCell;

		context.add(this);
		context.remove(hippie);

		System.out.println("Human has changed their electric car for a normal one " + this.getName());
	}

	/**
	 * Constructor used when a non-car user buys an electric car
	 * @param context
	 * @param human
	 */
	public Normal(Context<Object> context, Human human) {
		super(context);
		this.context = human.context;

		this.carType = "a";
		this.carUser = true;
		this.name = human.name;
		this.carAge = human.carAge;
		this.income = human.income;
		this.initialIncome = human.initialIncome;
		this.happiness = 1;
		this.carAgeExpire = RandomHelper.nextIntFromTo(8, 20);

		this.environmentalAwareness = human.environmentalAwareness;
		this.greediness = human.greediness;
		this.conformity = human.conformity;
		this.eProb = human.eProb;
		this.hedonism = human.hedonism;
		this.achievement = human.achievement;
		this.tradition = human.tradition;

		this.hasParked = false;
		this.location = human.location;
		this.adjacent = human.adjacent;
		this.parkingSpot = human.parkingSpot;
		this.closestCell = human.closestCell;

		context.add(this);
		context.remove(human);

	}

	/**
	 * Tells all agents to park their car daily
	 */
	@ScheduledMethod(start = 1, interval = 1, priority = 1, shuffle = true)
	public void parkCar() {
		// do we even use a car?
		if (!carUser)
			return;

		// Find best empty spot
		// Get location of home
		Grid<Object> grid = (Grid<Object>) context.getProjection("dwellingsGrid");
		Network<Object> livingin = (Network<Object>) context.getProjection("livingin");

		Iterable<Object> home = this.adjacent;
		GridPoint homeLocation = grid.getLocation(home.iterator().next());

		// Now look for parking spots
		Grid<Object> parkingSpots = (Grid<Object>) context.getProjection("parkingspacesGrid");

		// get all cells around the home location
		GridCellNgh<ParkingSpace> nghCreator = new GridCellNgh<ParkingSpace>(parkingSpots, homeLocation,
				ParkingSpace.class, 100, 100);

		List<GridCell<ParkingSpace>> gridCells = nghCreator.getNeighborhood(true);
		SimUtilities.shuffle(gridCells, RandomHelper.getUniform());

		// Check the neighbourhood for the closest empty spot
		// The spot has either no agent in it or an agent that isn't parked
		double minDistance = Double.MAX_VALUE, parkDistance = 0;
		double maxparkDistance = 0;
		GridCell<ParkingSpace> closestCell = null;
		ParkingSpace closestSpot = null;
		for (GridCell<ParkingSpace> cell : gridCells) {

			ParkingSpace spot = (ParkingSpace) parkingSpots.getObjectAt(cell.getPoint().getX(), cell.getPoint().getY());

			if (!spot.getOccupied() && this.carType == spot.getType()) {
				parkDistance = parkingSpots.getDistance(homeLocation, cell.getPoint());
				if (parkDistance < minDistance) {
					minDistance = parkDistance;
					closestCell = cell;
					closestSpot = spot;
				}
			}
		}

		// If we have found a parking spot: park!
		if (closestCell != null) {
			// normal cars can only park on spots of type "a"
			parkOnSpot(parkingSpots, parkDistance, closestCell, closestSpot);
			updateHappinessHappy(parkDistance);
		} else {
			// if the car wasn't able to park : subtract hedonism
			updateHappinessSad();

		}
		// Makes sure that the happiness stays within the -1 and 1 bounds
		if (this.happiness > 1)
			this.happiness = 1;
		if (this.happiness < -1)
			this.happiness = -1;
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST HAS HAPPINESS " + this.happiness);
		}

	}

	/**
	 * Updates the happiness when the agent was not able to park
	 */
	private void updateHappinessSad() {
		if (this.happiness >= -1)
			this.happiness += (float) -this.getAchievement() / 10 - this.getHedonism() / 10
					+ this.getEnvirA() * (15.23 - Neighbourhood.pollution) / 10;
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST WAS NOT ABLE TO FIND A SPOT. Happiness decreased by "
					+ (this.getAchievement() / 10 - this.getHedonism() / 10));
		}
	}

	/**
	 * Updates the happiness when the agent is able to park
	 * @param parkDistance
	 */
	private void updateHappinessHappy(double parkDistance) {
		if (this.happiness <= 1 && this.happiness >= -1)
			this.happiness += (float) this.getEnvirA() * (15.23 - Neighbourhood.pollution) / 10
			- this.getHedonism() / 1000 * parkDistance * parkDistance + this.getAchievement() / 10;

	}

	/**
	 * Moves the car to the closest spot to home 
	 * @param parkingSpots
	 * @param parkDistance
	 * @param closestCell
	 * @param closestSpot
	 */
	private void parkOnSpot(Grid<Object> parkingSpots, double parkDistance, GridCell<ParkingSpace> closestCell,
			ParkingSpace closestSpot) {
		if (this.getName().equals("0")) {
			System.out.println(" THE AGENT OF INTEREST HAS FOUND A SPOT. Happiness increased by "
					+ (-this.getHedonism() / 1000 * parkDistance * parkDistance + this.getAchievement() / 10));
			System.out.println(" Park distance: " + parkDistance);
		}
		parkingSpots.moveTo(this, closestCell.getPoint().getX(), closestCell.getPoint().getY());
		this.location = parkingSpots.getLocation(this);
		closestSpot.setOccupied(true);
		this.hasParked = true;
		this.closestCell = closestCell;
		this.parkingSpot = closestSpot;
	}

	public int getHuman() {
		return context.getObjects(Normal.class).size();
	}
}
